/**
  * Propiedad BOSOCIAL 2020
*/

'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var UserSchema = Schema({
    name: String,
    surname: String,
    email: String,
    t_identificacion: String,
    n_identificacion: String,
    nick: String,
    tipo_servicio: String,
    servicios: String,
    ciudad: String,
    localidad: String,
    web: String,
    password: String,
    role: String,
    image: String,
    portada: String, 
    telefono: String, 
    facebook: String, 
    twitter: String
});

UserSchema.index({'$**': 'servicios'});
UserSchema.index({'$**': 'name'});
UserSchema.index({'$**': 'surname'});

module.exports = mongoose.model('User', UserSchema);