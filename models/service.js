/**
  * Propiedad BOSOCIAL 2020
*/

'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ServiceSchema = Schema({
    title: String,
    description: String,
    date: String,
    cost: String,
    accept: String,
    backgroundColor: String,
    created_at: String,
    userContratista: { type: Schema.ObjectId, ref: 'User' },
    userContratante: { type: Schema.ObjectId, ref: 'User' },
});

module.exports = mongoose.model('Service', ServiceSchema);