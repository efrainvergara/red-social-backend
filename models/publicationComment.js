/**
  * Propiedad BOSOCIAL 2020
*/

'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PublicationCommentSchema = Schema({
    comment: String,
    created_at: String,
    publication: { type: Schema.ObjectId, ref: 'Publication' },
    user: { type: Schema.ObjectId, ref: 'User' }
});

module.exports = mongoose.model('PublicationComment', PublicationCommentSchema);