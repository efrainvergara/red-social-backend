/**
  * Propiedad BOSOCIAL 2020
*/

'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PublicationSchema = Schema({
  text: String,
  file: String,
  created_at: String,
  user: { type: Schema.ObjectId, ref: 'User' },
  userOwn: { type: Schema.ObjectId, ref: 'User' },
  likes: { type: Number, default: 0 },
  likedBy: { type: Array },
  comments: [{
    comment: { type: String },
    commentatorId: { type: Schema.ObjectId, ref: 'User' },
    created_at: { type: String },
  }],
  isShared: Boolean
});

module.exports = mongoose.model('Publication', PublicationSchema);