/**
  * Propiedad BOSOCIAL 2020
*/

'use strict'

var express = require('express');
var PublicationCommentController = require('../controllers/publicationComment');
var api = express.Router();

var md_auth = require('../middlewares/authenticated');

var multipart = require('connect-multiparty');
var md_upload = multipart({ uploadDir: './uploads/publications'});

api.get('/probando-comentario', PublicationCommentController.probando);
api.post('/publication-comment/:publication', md_auth.ensureAuth, PublicationCommentController.savePublication);
api.get('/publication-comment/:id', md_auth.ensureAuth, PublicationCommentController.getCommentPublication);
api.get('/publications-comment/:publication/:page?', md_auth.ensureAuth, PublicationCommentController.getCommentsPublication);
api.delete('/publication-comment/:id', md_auth.ensureAuth, PublicationCommentController.deleteComment);
api.put('/publication-comment/:id', md_auth.ensureAuth, PublicationCommentController.updateComment);
module.exports = api;