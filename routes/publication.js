/**
  * Propiedad BOSOCIAL 2020
*/

'use strict'

var express = require('express');
var PublicationController = require('../controllers/publication');
var api = express.Router();

var md_auth = require('../middlewares/authenticated');

var multipart = require('connect-multiparty');
var md_upload = multipart({ uploadDir: './uploads/publications' });

var crypto = require('crypto')
var multer = require('multer');

const storage = multer.diskStorage({
    destination(req, file, cb) {
        cb(null, './uploads/publications');
    },
    filename(req, file = {}, cb) {
        const { originalname } = file;
        const fileExtension = (originalname.match(/\.+[\S]+$/) || [])[0];
        cb(null, `${file.fieldname}__${Date.now()}${fileExtension}`);
    },
});
const upload = multer({ storage });

api.get('/probando', md_auth.ensureAuth, PublicationController.probando);
api.post('/publication', md_auth.ensureAuth, PublicationController.savePublication);
api.get('/publications/:page?', md_auth.ensureAuth, PublicationController.getPublications);
api.get('/publications-user/:user/:page?', md_auth.ensureAuth, PublicationController.getPublicationsUser);
api.get('/publication/:id', md_auth.ensureAuth, PublicationController.getPublication);
api.delete('/publication/:id', md_auth.ensureAuth, PublicationController.deletePublication);
api.post('/upload-image-pub/:id', [md_auth.ensureAuth, upload.single('image')], PublicationController.uploadImage);
api.get('/get-image-pub/:imageFile', PublicationController.getImageFile);
api.put('/publication/:id', md_auth.ensureAuth, PublicationController.updatePublication);
api.put('/publication-like/:id', md_auth.ensureAuth, PublicationController.likePublication);
api.post('/comment/:id', md_auth.ensureAuth, PublicationController.commentPublication);
api.put('/delete-comment', md_auth.ensureAuth, PublicationController.deleteComment);
// api.post('/publication-share/:id', md_auth.ensureAuth, PublicationController.savePublicationShared);

module.exports = api;