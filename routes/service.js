/**
  * Propiedad BOSOCIAL 2020
*/

'use strict'

var express = require('express');
var service = require('../controllers/service');
var api = express.Router();

var md_auth = require('../middlewares/authenticated');

api.get('/probando-service', md_auth.ensureAuth, service.probando);
api.post('/service-create/:id', md_auth.ensureAuth, service.saveService);
api.get('/contractor/:page?', md_auth.ensureAuth, service.getServicesContractor);
api.get('/provider/:page?', md_auth.ensureAuth, service.getServicesProvider);
api.get('/date-provider/:userProvider/:page?', md_auth.ensureAuth, service.getServicesProviderWithId);
api.put('/accept-service/:id', md_auth.ensureAuth, service.acceptService);
api.put('/recjet-service/:id', md_auth.ensureAuth, service.recjetService);

module.exports = api;