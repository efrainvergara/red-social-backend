/**
  * Propiedad BOSOCIAL 2020
*/

'use strict'

var express = require('express');
var UserController = require('../controllers/user');

var api = express.Router();
var md_auth = require('../middlewares/authenticated');

var multipart = require('connect-multiparty');
var md_upload = multipart({ uploadDir: './uploads/users' });
var md_upload_portada = multipart({ uploadDir: './uploads/portadas' })

var crypto = require('crypto')

var multer = require('multer');

const storageUser = multer.diskStorage({

    destination(req, file, cb) {

        cb(null, './uploads/users');

    },

    filename(req, file = {}, cb) {

        const { originalname } = file;



        const fileExtension = (originalname.match(/\.+[\S]+$/) || [])[0];

        // cb(null, `${file.fieldname}__${Date.now()}${fileExtension}`);

        crypto.pseudoRandomBytes(16, function (err, raw) {

            cb(null, raw.toString('hex') + Date.now() + fileExtension);

        });

    },

});

var mul_upload = multer({ dest: './uploads/users', storageUser });

const storage = multer.diskStorage({
    destination(req, file, cb) {
        cb(null, './uploads/users');
    },
    filename(req, file = {}, cb) {
        const { originalname } = file;
        const fileExtension = (originalname.match(/\.+[\S]+$/) || [])[0];
        cb(null, `${file.fieldname}__${Date.now()}${fileExtension}`);
    },
});
const upload = multer({ storage });


const storagePortada = multer.diskStorage({
    destination(req, file, cb) {
        cb(null, './uploads/portadas');
    },
    filename(req, file = {}, cb) {
        const { originalname } = file;
        const fileExtension = (originalname.match(/\.+[\S]+$/) || [])[0];
        cb(null, `${file.fieldname}__${Date.now()}${fileExtension}`);
    },
});
const uploadPortada = multer({ storagePortada });

api.get('/home', UserController.home);
api.get('/pruebas', md_auth.ensureAuth, UserController.pruebas);
api.post('/register', UserController.saveUser);
api.post('/login', UserController.loginUser);
api.get('/user/:id', md_auth.ensureAuth, UserController.getUser);
api.get('/users/:page?', md_auth.ensureAuth, UserController.getUsers);
api.get('/counters/:id?', md_auth.ensureAuth, UserController.getCounters)
api.put('/update-user/:id', md_auth.ensureAuth, UserController.updateUser);
api.post('/upload-image-user/:id', [md_auth.ensureAuth, upload.single('image')], UserController.uploadImage);
api.post('/upload-portada-user/:id', [md_auth.ensureAuth, upload.single('image')], UserController.uploadPortada);
api.get('/get-image-user/:imageFile', UserController.getImageFile);
api.get('/get-portada-user/:imageFile', UserController.getPortadaFile);
api.post('/search', md_auth.ensureAuth, UserController.globalSearch);
api.put('/change-password/:id', md_auth.ensureAuth, UserController.changePassword);

module.exports = api;