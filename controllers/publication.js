/**
  * Propiedad BOSOCIAL 2020
*/

'use strict'

var path = require('path');
var fs = require('fs');
var moment = require('moment');
var mongoosePaginate = require('mongoose-pagination');

var Publication = require('../models/publication');
var User = require('../models/user');
var Follow = require('../models/follow');
const publication = require('../models/publication');

function probando(req, res) {
    res.status(200).send({
        message: "Hola desde el controlador de publicaciones"
    });
}

function savePublication(req, res) {
    var params = req.body;

    if (!params.text) return res.status(200).send({ message: 'Debes enviar un texto!!' });

    var publication = new Publication();
    publication.text = params.text;
    publication.isShared = params.isShared;
    publication.file = 'null';
    publication.user = req.user.sub;
    publication.userOwn = params.userOwn;
    publication.created_at = moment().unix();

    publication.save((err, publicationStored) => {
        if (err) return res.status(500).send({ message: 'Error al guardar la publicacion' });

        if (!publicationStored) return res.status(500).send({ message: 'la publicacion no ha sido guardada' });

        return res.status(200).send({ publication: publicationStored });

    })
}

// function savePublicationShared(req, res) {
//     // Search the database with id
//     publication.findOne({ _id: req.params.id }, (err, publication) => {
//         // Check if error was encountered
//         if (err) {
//             res.json({ success: false, message: 'Invalid publication id' }); // Return error message
//         } else {
//             // Check if id matched the id of a publication post in the database
//             if (!publication) {
//                 res.json({ success: false, message: 'That publication was not found.' }); // Return error message
//             } else {
//                 // Get data from user that is signed 
//                 var userId = req.user.sub;
//                 User.findOne({ _id: userId }, (err, user) => {
//                     // Check if error was found
//                     if (err) {
//                         res.json({ success: false, message: 'Something went wrong.' }); // Return error message
//                     }
//                     else {
//                         publication.shared.push({
//                             sharerId: req.user.sub, // Person who shared
//                             created_at: moment().unix(),
//                             isShared: true
//                         });
//                         // Save publication post
//                         publication.save((err) => {
//                             if (err) {
//                                 res.json({ success: false, message: 'Something went wrong.' }); // Return error message
//                             } else {
//                                 res.json({ success: true, message: 'publication Shared!' }); // Return success message
//                             }
//                         });

//                     }
//                 });
//             }
//         }
//     });
// }

function getPublications(req, res) {
    var page = 1;

    if (req.params.page) {
        page = req.params.page;
    }

    var itemsPerPage = 20;

    Follow.find({ user: req.user.sub }).populate('followed').exec((err, follows) => {
        if (err) return res.status(500).send({ message: 'Error al devolver el seguimiento' });

        var follows_clean = [];

        follows.forEach((follow) => {
            follows_clean.push(follow.followed);
        });

        follows_clean.push(req.user.sub);

        Publication.find({ user: { "$in": follows_clean } }).sort('-created_at').populate('user').populate('userOwn').populate('comments.commentatorId').paginate(page, itemsPerPage, (err, publications, total) => {
            if (err) return res.status(500).send({ message: 'Error al devolver publicaciones' });

            if (!publications) return res.status(404).send({ message: 'No hay publicaciones' });

            return res.status(200).send({
                total_items: total,
                pages: Math.ceil(total / itemsPerPage),
                page: page,
                items_per_page: itemsPerPage,
                publications
            });
        });
    });
}

function getPublicationsUser(req, res) {
    var page = 1;

    if (req.params.page) {
        page = req.params.page;
    }

    var user = req.user.sub;
    if (req.params.user) {
        user = req.params.user
    }

    var itemsPerPage = 20;

    Publication.find({ user: user }).sort('-created_at').populate('user').populate('userOwn').populate('comments.commentatorId').paginate(page, itemsPerPage, (err, publications, total) => {
        if (err) return res.status(500).send({ message: 'Error al devolver publicaciones' });

        if (!publications) return res.status(404).send({ message: 'No hay publicaciones' });

        return res.status(200).send({
            total_items: total,
            pages: Math.ceil(total / itemsPerPage),
            page: page,
            items_per_page: itemsPerPage,
            publications
        });
    });
}

function getPublication(req, res) {
    var publicationId = req.params.id;

    Publication.findById(publicationId, (err, publication) => {
        if (err) return res.status(500).send({ message: 'Error al devolver la publicacion' });

        if (!publication) return res.status(404).send({ message: 'No existe publicacion' });

        return res.status(200).send({ publication });
    });
}

function deletePublication(req, res) {
    var publicationId = req.params.id;
    //deleteOne or remove
    Publication.find({ 'user': req.user.sub, '_id': publicationId }).deleteOne((err) => {
        if (err) return res.status(500).send({ message: 'Error al eliminar la publicacion' });

        return res.status(200).send({ message: 'Publicacion Eliminada correctamente' });
    })
}

//Subir archivos de imagen/avatar publicacion
function uploadImage(req, res) {
    var publicationId = req.params.id;
    console.log(req.file)
    if (req.file) {
        var file_path = req.file.path;

        var file_split = file_path.split('\\');

        var file_name = file_split[2];

        var ext_split = file_name.split('\.');
        var file_ext = ext_split[1];

        if (file_ext == 'png' || file_ext == 'jpg' || file_ext == 'jpeg' || file_ext == 'gif' || file_ext == 'mp4' || file_ext == 'avi' || file_ext == 'wmv') {

            Publication.findOne({ 'user': req.user.sub, '_id': publicationId }).exec((err, publication) => {

                if (publication) {
                    //actualizar dpcumento de usuario logueado
                    Publication.findByIdAndUpdate(publicationId, { file: file_name }, { new: true }, (err, publicationUpdated) => {
                        if (err) return res.status(500).send({ message: 'Error en la peticion' });

                        if (!publicationUpdated) return res.status(404).send({ message: 'No se ha podido actualizar el usuario' });

                        return res.status(200).send({ publication: publicationUpdated });
                    });
                } else {
                    return removeFilesOfUploads(res, file_path, 'No tienes permiso para actualizar la publicacion');
                }
            });


        } else {
            return removeFilesOfUploads(res, file_path, 'Extension no valida');
        }

    } else {
        return res.status(200).send({ message: 'No se han subido imagenes' });
    }
}

function removeFilesOfUploads(res, file_path, message) {
    fs.unlink(file_path, (err) => {
        return res.status(200).send({ message: message });
    });
}

//devolver imagen usuario
function getImageFile(req, res) {
    var image_file = req.params.imageFile;
    var path_file = './uploads/publications/' + image_file;

    fs.exists(path_file, (exists) => {
        if (exists) {
            res.sendFile(path.resolve(path_file));
        } else {
            res.status(200).send({ message: 'No existe la imagen...' });
        }
    });

}

//Editar Comentario
function updatePublication(req, res) {
    var publicationId = req.params.id;
    var update = req.body;

    Publication.findByIdAndUpdate(publicationId, update, { new: true }, (err, publicationUpdated) => {
        if (err) return res.status(500).send({ message: 'Error en la peticion' });

        if (!publicationUpdated) return res.status(404).send({ message: 'No se ha podido actualizar la publicacion' });

        return res.status(200).send({ publication: publicationUpdated });
    });
}

function likePublication(req, res) {
    // Search the database with id
    publication.findOne({ _id: req.params.id }, (err, publication) => {
        // Check if error was encountered
        if (err) {
            res.json({ success: false, message: 'Invalid publication id' }); // Return error message
        } else {
            // Check if id matched the id of a publication post in the database
            if (!publication) {
                res.json({ success: false, message: 'That publication was not found.' }); // Return error message
            } else {
                // Get data from user that is signed 
                var userId = req.user.sub;
                User.findOne({ _id: userId }, (err, user) => {
                    // Check if error was found
                    if (err) {
                        res.json({ success: false, message: 'Something went wrong.' }); // Return error message
                    }
                    // Check if id of user in session was found in the database
                    else {
                        // Check if the user who liked the post has already liked the publication post before
                        if (publication.likedBy.includes(user._id)) {
                            publication.likes--; // Reduce the total number of likes
                            publication.likedBy.remove(user._id); // delete liker's _id into array of likedBy
                            // Save publication post data
                            publication.save((err) => {
                                // Check if error was found
                                if (err) {
                                    res.json({ success: false, message: 'Something went wrong.' }); // Return error message
                                } else {
                                    res.json({ success: true, message: 'publication disliked!' }); // Return success message
                                }
                            });
                        } else {
                            publication.likes++; // Incriment likes
                            publication.likedBy.push(user._id); // Add liker's _id into array of likedBy
                            // Save publication post
                            publication.save((err) => {
                                if (err) {
                                    res.json({ success: false, message: 'Something went wrong.' }); // Return error message
                                } else {
                                    res.json({ success: true, message: 'publication liked!' }); // Return success message
                                }
                            });
                        }
                    }
                });
            }
        }
    });
}

function commentPublication(req, res) {
    // Search the database with id
    publication.findOne({ _id: req.params.id }, (err, publication) => {
        // Check if error was encountered
        if (err) {
            res.json({ success: false, message: 'Invalid publication id' }); // Return error message
        } else {
            // Check if id matched the id of a publication post in the database
            if (!publication) {
                res.json({ success: false, message: 'That publication was not found.' }); // Return error message
            } else {

                if (!req.body.comment) return res.status(200).send({ message: 'Debes enviar un comentario!!' });

                publication.comments.push({
                    comment: req.body.comment, // Comment field
                    commentatorId: req.user.sub, // Person who commented
                    created_at: moment().unix()
                });

                publication.save((err, commentStored) => {
                    if (err) return res.status(500).send({ message: 'Error al guardar el comentario' });

                    if (!commentStored) return res.status(500).send({ message: 'el comentario no ha sido guardado' });

                    return res.status(200).send({ comentario: commentStored });

                })
            }
        }
    });
}

function deleteComment(req, res) {
    // Search the database with id
    publication.findOne({ _id: req.body.publication }, (err, publication) => {
        // Check if error was encountered
        if (err) {
            res.json({ success: false, message: 'Invalid publication id' }); // Return error message
        } else {
            // Check if id matched the id of a publication post in the database
            if (!publication) {
                res.json({ success: false, message: 'That publication was not found.' }); // Return error message
            } else {
                var commentId = req.body.comment

                publication.comments.remove(commentId); // delete liker's _id into array of likedBy
                // Save publication post data
                publication.save((err) => {
                    // Check if error was found
                    if (err) {
                        res.json({ success: false, message: 'Something went wrong.' }); // Return error message
                    } else {
                        res.json({ success: true, message: 'Comentario Eliminado!' }); // Return success message
                    }
                });

            }
        }
    });
}

module.exports = {
    probando,
    savePublication,
    getPublications,
    getPublicationsUser,
    getPublication,
    deletePublication,
    uploadImage,
    getImageFile,
    updatePublication,
    likePublication,
    commentPublication,
    deleteComment
}