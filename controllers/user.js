/**
  * Propiedad BOSOCIAL 2020
*/

'use strict'
var bcrypt = require('bcrypt-nodejs');
var mongoosePaginate = require('mongoose-pagination');
var fs = require('fs');
var path = require('path');

var User = require('../models/user');
var Follow = require('../models/follow');
var Publication = require('../models/publication');
var jwt = require('../services/jwt');

// metodos de prueba
function home(req, res) {
    res.status(200).send({
        message: 'Hola mundo'
    });
};

function pruebas(req, res) {
    console.log(req.body);
    res.status(200).send({
        message: 'Accion de pruebas en el servidor de NodeJS'
    });
};

// Registro
function saveUser(req, res) {
    var params = req.body;
    var user = new User();

    if (params.name && params.surname && params.email && params.password) {

        user.name = params.name;
        user.surname = params.surname;
        user.email = params.email;
        user.telefono = params.telefono;
        user.t_identificacion = params.t_identificacion;
        user.n_identificacion = params.n_identificacion;
        user.nick = params.nick;
        user.tipo_servicio = params.tipo_servicio;
        user.servicios = params.servicios;
        user.ciudad = params.ciudad;
        user.localidad = params.localidad;
        user.web = params.web;
        user.role = 'ROLE_USER';
        user.image = null;
        user.portada = null;

        // Comprobar usuarios duplicados
        User.find({
            $or: [
                { email: user.email.toLowerCase() }
                // { n_identificacion: user.n_identificacion.toLowerCase() }
            ]
        }).exec((err, users) => {
            if (err) return res.status(500).send({ message: 'Error la peticion de usuarios' });

            if (users && users.length > 1) {
                return res.status(200).send({ message: 'El usuario ya existe' });
            } else {
                // Cifrar contraseña
                bcrypt.hash(params.password, null, null, (err, hash) => {
                    user.password = hash;

                    user.save((err, userStored) => {
                        if (err) return res.status(500).send({ message: 'Error al guardar el usuario' });

                        if (userStored) {
                            res.status(200).send({ user: userStored });
                        } else {
                            res.status(404).send({ message: 'No se ha registrado el usuario' });
                        }
                    });
                });
            }
        })
    } else {
        res.status(200).send({
            message: 'Envia todos los campos necesarios!!'
        });
    }
}

// Login
function loginUser(req, res) {
    var params = req.body;

    var email = params.email;
    var password = params.password;

    User.findOne({ email: email }, (err, user) => {
        if (err) return res.status(500).send({ message: 'Error en la peticion' });

        if (user) {
            bcrypt.compare(password, user.password, (err, check) => {
                if (check) {
                    if (params.gettoken) {
                        // generar y devolver el token
                        return res.status(200).send({
                            token: jwt.createToken(user)
                        });
                    } else {
                        // devolver datos de usuario
                        user.password = undefined;
                        return res.status(200).send({ user });
                    }
                } else {
                    return res.status(404).send({ message: 'El usuario no se ha podido identificar' });
                }
            })
        } else {
            return res.status(404).send({ message: 'El usuario no se ha podido identificar!!' });
        }
    })
}

// ver un usuario por id
function getUser(req, res) {
    var userId = req.params.id;

    User.findById(userId, (err, user) => {
        if (!user) return res.status(404).send({ message: "User Not Found." });
        if (err) return res.status(500).send({ message: "Request Error." });

        followThisUser(req.user.sub, userId).then((value) => {
            return res.status(200).send({
                user,
                following: value.following,
                followed: value.followed
            });
        });
    });
}

async function followThisUser(identity_user_id, user_id) {
    var following = await Follow.findOne({ user: identity_user_id, followed: user_id }).exec()
        .then((following) => {
            return following;
        })
        .catch((err) => {
            return handleError(err);
        });

    var followed = await Follow.findOne({ user: user_id, followed: identity_user_id }).exec()
        .then((followed) => {
            return followed;
        })
        .catch((err) => {
            return handleError(err);
        });

    return {
        following: following,
        followed: followed
    };
}

//listar usuarios paginado
function getUsers(req, res) {
    var identity_user_id = req.user.sub;

    var page = 1;
    if (req.params.page) {
        page = req.params.page;
    }

    var itemsPerPage = 5;

    User.find().sort('_id').paginate(page, itemsPerPage, (err, users, total) => {
        if (err) return res.status(500).send({ message: 'Error en la peticion' });

        if (!users) return res.status(404).send({ message: 'No hay usuarios disponibles' });

        followUserIds(identity_user_id).then((value) => {
            return res.status(200).send({
                users,
                users_following: value.following,
                users_follow_me: value.followed,
                total,
                pages: Math.ceil(total / itemsPerPage)
            });
        });
    });
}

async function followUserIds(user_id) {
    var following = await Follow.find({ "user": user_id }).select({ '_id': 0, '__v': 0, 'user': 0 }).exec()
        .then((follows) => {
            return follows;
        });

    var followed = await Follow.find({ "followed": user_id }).select({ '_id': 0, '__v': 0, 'followed': 0 }).exec()
        .then((follows) => {
            return follows;
        });

    //procesar following ids
    var following_clean = [];

    following.forEach((follow) => {
        following_clean.push(follow.followed);
    });

    //procesar followed ids
    var followed_clean = [];

    followed.forEach((follow) => {
        followed_clean.push(follow.user);
    });

    return {
        following: following_clean,
        followed: followed_clean
    }
}

function getCounters(req, res) {
    var userId = req.user.sub;

    if (req.params.id) {
        userId = req.params.id;
    }

    getCountFollow(userId).then((value) => {
        return res.status(200).send(value);
    });
}

async function getCountFollow(user_id) {
    var following = await Follow.count({ user: user_id }).exec()
        .then((following) => {
            return following;
        })
        .catch((err) => {
            return handleError(err);
        });

    var followed = await Follow.count({ followed: user_id }).exec()
        .then((followed) => {
            return followed;
        })
        .catch((err) => {
            return handleError(err);
        });

    var publications = await Publication.count({ user: user_id }).exec()
        .then((followed) => {
            return followed;
        })
        .catch((err) => {
            return handleError(err);
        });

    return {
        following: following,
        followed: followed,
        publications: publications
    }
}

//Editar info usuario
function updateUser(req, res) {
    var userId = req.params.id;
    var update = req.body;


    //borrar propiedad password
    delete update.password;

    if (userId != req.user.sub) {
        return res.status(500).send({ message: 'No tienes permisos para actualizar los datos del usuario' });
    }

    User.find({
        $or: [
            { email: update.email.toLowerCase() },
            { nick: update.nick.toLowerCase() }
        ]
    }).exec((err, users) => {
        var user_isset = false;
        users.forEach((user) => {
            if (user && user._id != userId) user_isset = true;
        });

        if (user_isset) return res.status(404).send({ message: 'Los datos ya estan en uso' });

        User.findByIdAndUpdate(userId, update, { new: true }, (err, userUpdated) => {
            if (err) return res.status(500).send({ message: 'Error en la peticion' });

            if (!userUpdated) return res.status(404).send({ message: 'No se ha podido actualizar el usuario' });

            return res.status(200).send({ user: userUpdated });
        });
    });
}

//Subir archivos de imagen/avatar usuario
function uploadImage(req, res) {
    var userId = req.params.id;
    console.log(req.file)
    if (req.file) {
        var file_path = req.file.path;
        console.log(file_path);

        var file_split = file_path.split('\\');
        console.log(file_split);

        var file_name = file_split[2];
        console.log(file_name);

        var ext_split = file_name.split('\.');
        var file_ext = ext_split[1];
        console.log(file_ext);

        if (userId != req.user.sub) {
            return removeFilesOfUploads(res, file_path, 'No tienes permisos para actualizar los datos del usuario');
        }

        if (file_ext == 'png' || file_ext == 'jpg' || file_ext == 'jpeg' || file_ext == 'gif') {
            //actualizar dpcumento de usuario logueado

            User.findByIdAndUpdate(userId, { image: file_name }, { new: true }, (err, userUpdated) => {
                if (err) return res.status(500).send({ message: 'Error en la peticion' });

                if (!userUpdated) return res.status(404).send({ message: 'No se ha podido actualizar el usuario' });

                return res.status(200).send({ user: userUpdated });
            })

        } else {
            return removeFilesOfUploads(res, file_path, 'Extension no valida');
        }

    } else {
        return res.status(200).send({ message: 'No se han subido imagenes' });
    }
}

//Subir archivos de imagen/avatar usuario
function uploadPortada(req, res) {
    var userId = req.params.id;
    console.log(req.file)
    if (req.file) {
        var file_path = req.file.path;
        console.log(file_path);

        var file_split = file_path.split('\\');
        console.log(file_split);

        var file_name = file_split[2];
        console.log(file_name);

        var ext_split = file_name.split('\.');
        var file_ext = ext_split[1];
        console.log(file_ext);

        if (userId != req.user.sub) {
            return removeFilesOfUploads(res, file_path, 'No tienes permisos para actualizar los datos del usuario');
        }

        if (file_ext == 'png' || file_ext == 'jpg' || file_ext == 'jpeg' || file_ext == 'gif') {
            //actualizar dpcumento de usuario logueado

            User.findByIdAndUpdate(userId, { portada: file_name }, { new: true }, (err, userUpdated) => {
                if (err) return res.status(500).send({ message: 'Error en la peticion' });

                if (!userUpdated) return res.status(404).send({ message: 'No se ha podido actualizar el usuario' });

                return res.status(200).send({ user: userUpdated });
            })

        } else {
            return removeFilesOfUploads(res, file_path, 'Extension no valida');
        }

    } else {
        return res.status(200).send({ message: 'No se han subido imagenes' });
    }
}

function removeFilesOfUploads(res, file_path, message) {
    fs.unlink(file_path, (err) => {
        return res.status(200).send({ message: message });
    });
}

//devolver imagen usuario
function getImageFile(req, res) {
    var image_file = req.params.imageFile;
    var path_file = './uploads/users/' + image_file;

    fs.exists(path_file, (exists) => {
        if (exists) {
            res.sendFile(path.resolve(path_file));
        } else {
            res.status(200).send({ message: 'No existe la imagen...' });
        }
    });

}

//devolver imagen usuario
function getPortadaFile(req, res) {
    var image_file = req.params.imageFile;
    var path_file = './uploads/users/' + image_file;

    fs.exists(path_file, (exists) => {
        if (exists) {
            res.sendFile(path.resolve(path_file));
        } else {
            res.status(200).send({ message: 'No existe la imagen...' });
        }
    });

}

function globalSearch(req, res) {
    var word = req.query.q;
    if(word.length > 0){
        User.find({
            $or: [
                { servicios: { $regex: '.*' + word + '.*', $options: 'i' } },
                { name: { $regex: '.*' + word + '.*', $options: 'i' } },
                { surname: { $regex: '.*' + word + '.*', $options: 'i' } },
            ]
        })
            .then(results => {
                var count = results.length
                res.status(200).json({
                    word,
                    count,
                    results
                })
            })
            .catch(err => {
                res.status(500).json({
                    error: err
                });
            });
    }else{
        return res.status(206).send({ message: 'El Query no debe ir vacio' });
    }
}
function changePassword(req, res) {
    var passwordDetails = req.body;
    User.findById(req.params.id, function (err, user) {
        if (user) {
            bcrypt.compare(passwordDetails.currentPassword, user.password, (err, check) => {
                if (check) {
                    if (passwordDetails.newPassword === passwordDetails.verifyPassword) {
                        user.password = passwordDetails.newPassword;

                        // Cifrar contraseña
                        bcrypt.hash(passwordDetails.newPassword, null, null, (err, hash) => {
                            user.password = hash;

                            user.save((err) => {
                                if (err) {
                                    return res.status(422).send({
                                        message: errorHandler.getErrorMessage(err)
                                    });
                                } else {
                                    res.send({
                                        message: 'Contraseña cambiada satisfactoriamente'
                                    });
                                }
                            });
                        });
                    }
                    else {
                        return res.status(404).send({ message: 'Las contraseñas no coinciden' });
                    }
                }
                else {
                    return res.status(404).send({ message: 'La contraseña actual no coincide' });
                }
            })
        } 
    });
};

module.exports = {
    home,
    pruebas,
    saveUser,
    loginUser,
    getUser,
    getUsers,
    getCounters,
    updateUser,
    uploadImage,
    getImageFile,
    uploadPortada,
    getPortadaFile,
    globalSearch,
    changePassword
}