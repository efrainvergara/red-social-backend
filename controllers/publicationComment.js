/**
  * Propiedad BOSOCIAL 2020
*/

'use strict'

var moment = require('moment');
var mongoosePaginate = require('mongoose-pagination');

var Publication = require('../models/publication');
var User = require('../models/user');
var Follow = require('../models/follow');
var PublicationComment = require('../models/publicationComment');

function probando(req, res) {
    res.status(200).send({
        message: "Hola desde el controlador de comentario de publicaciones"
    });
}

function savePublication(req, res) {
    var params = req.body;

    if (!params.comment) return res.status(200).send({ message: 'Debes enviar un comentario!!' });

    var publicationComment = new PublicationComment();
    publicationComment.comment = params.comment;
    publicationComment.user = req.user.sub;
    publicationComment.publication = req.params.publication;
    publicationComment.created_at = moment().unix();

    publicationComment.save((err, commentStored) => {
        if (err) return res.status(500).send({ message: 'Error al guardar el comentario' });

        if (!commentStored) return res.status(500).send({ message: 'el comentario no ha sido guardado' });

        return res.status(200).send({ comentario: commentStored });

    })
}

function getCommentPublication(req, res) {
    var commentId = req.params.id;

    PublicationComment.findById(commentId, (err, comment) => {
        if (err) return res.status(500).send({ message: 'Error al devolver el comentario' });

        if (!comment) return res.status(404).send({ message: 'No existe el comentario' });

        return res.status(200).send({ comment });
    });
}

function getCommentsPublication(req, res) {

    var publication = req.params.publication;
    
    var page = 1;

    if (req.params.page) {
        page = req.params.page;
    }

    var itemsPerPage = 4;

    PublicationComment.find({ publication: publication }).sort('-created_at').paginate(page, itemsPerPage, (err, comments, total) => {
        if (err) return res.status(500).send({ message: 'Error al devolver comentarios' });

        if (!comments) return res.status(404).send({ message: 'No hay comentarios' });

        return res.status(200).send({
            total_items: total,
            pages: Math.ceil(total / itemsPerPage),
            page: page,
            items_per_page: itemsPerPage,
            comments
        });
    });
}

function deleteComment(req, res) {
    var commentId = req.params.id;
    //deleteOne or remove
    PublicationComment.find({ 'user': req.user.sub, '_id': commentId }).deleteOne((err) => {
        if (err) return res.status(500).send({ message: 'Error al eliminar el comentario' });

        return res.status(200).send({ message: 'Comentario Eliminado correctamente' });
    })
}

//Editar Comentario
function updateComment(req, res) {
    var commentId = req.params.id;
    var update = req.body;

    PublicationComment.findByIdAndUpdate(commentId, update, { new: true }, (err, commentUpdated) => {
        if (err) return res.status(500).send({ message: 'Error en la peticion' });

        if (!commentUpdated) return res.status(404).send({ message: 'No se ha podido actualizar el comentario' });

        return res.status(200).send({ comment: commentUpdated });
    });
}
module.exports = {
    probando,
    savePublication,
    getCommentPublication,
    getCommentsPublication,
    deleteComment,
    updateComment
}