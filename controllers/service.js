/**
  * Propiedad BOSOCIAL 2020
*/

"use strict";

var path = require("path");
var fs = require("fs");
var moment = require("moment");
var mongoosePaginate = require("mongoose-pagination");

var service = require("../models/service");
var User = require("../models/user");
var Follow = require("../models/follow");
var Service = require("../models/service");

function probando(req, res) {
  res.status(200).send({
    message: "Hola desde el controlador de servicios",
  });
}

function saveService(req, res) {
  var idUser = req.params.id;
  User.findOne({ _id: idUser }, (err, user) => {
    // Check if error was encountered
    if (err) {
      res.json({ success: false, message: "Invalid user id" }); // Return error message
    } else {
      // Check if id matched the id of a user post in the database
      if (!user) {
        res.json({ success: false, message: "User id was not found." }); // Return error message
      } else {
        var params = req.body;

        if (!params.description)
          return res
            .status(200)
            .send({ message: "Debes enviar una descripcion!!" });

        var parDate = params.date + " " + params.hour;

        var service = new Service();
        service.title = params.title;
        service.description = params.description;
        service.date = parDate;
        service.cost = params.cost;
        service.accept = params.accept;
        service.backgroundColor = params.backgroundColor;
        service.userContratista = req.user.sub;
        service.userContratante = idUser;
        service.created_at = moment().unix();

        service.save((err, serviceStored) => {
          if (err)
            return res
              .status(500)
              .send({ message: "Error al guardar el servicio" });

          if (!serviceStored)
            return res
              .status(500)
              .send({ message: "El servicio ha sido guardado correctamente" });

          return res.status(200).send({ service: serviceStored });
        });
      }
    }
  });
}

function getServicesContractor(req, res) {
  var page = 1;

  if (req.params.page) {
    page = req.params.page;
  }

  var itemsPerPage = 10000;

  Service.find({ userContratista: req.user.sub })
    .sort("-created_at")
    .populate("userContratista")
    .populate("userContratante")
    .paginate(page, itemsPerPage, (err, contractors, total) => {
      if (err)
        return res.status(500).send({ message: "Error al devolver datos" });

      if (!contractors)
        return res.status(404).send({ message: "No hay publicaciones" });

      return res.status(200).send({
        total_items: total,
        pages: Math.ceil(total / itemsPerPage),
        page: page,
        items_per_page: itemsPerPage,
        contractors,
      });
    });
}

function getServicesProvider(req, res) {
  var page = 1;

  if (req.params.page) {
    page = req.params.page;
  }

  var itemsPerPage = 10000;

  Service.find({ userContratante: req.user.sub })
    .sort("-created_at")
    .populate("userContratista")
    .populate("userContratante")
    .paginate(page, itemsPerPage, (err, providers, total) => {
      if (err)
        return res.status(500).send({ message: "Error al devolver datos" });

      if (!providers)
        return res.status(404).send({ message: "No hay publicaciones" });

      return res.status(200).send({
        total_items: total,
        pages: Math.ceil(total / itemsPerPage),
        page: page,
        items_per_page: itemsPerPage,
        providers,
      });
    });
}

function getServicesProviderWithId(req, res) {
  var page = 1;

  if (req.params.page) {
    page = req.params.page;
  }

  var itemsPerPage = 10000;

  Service.find({ userContratante: req.params.userProvider })
    .sort("-created_at")
    .populate("userContratista")
    .populate("userContratante")
    .paginate(page, itemsPerPage, (err, providers, total) => {
      if (err)
        return res.status(500).send({ message: "Error al devolver datos" });

      if (!providers)
        return res.status(404).send({ message: "No hay servicios" });

      return res.status(200).send({
        total_items: total,
        pages: Math.ceil(total / itemsPerPage),
        page: page,
        items_per_page: itemsPerPage,
        providers,
      });
    });
}

//Editar Comentario
function acceptService(req, res) {
  // Search the database with id
  Service.findOne({ _id: req.params.id }, (err, service) => {
    // Check if error was encountered
    if (err) {
      res.json({ success: false, message: "Invalid service id" }); // Return error message
    } else {
      // Check if id matched the id of a service post in the database
      if (!service) {
        res.json({ success: false, message: "That service was not found." }); // Return error message
      } else {
        // Get data from user that is signed
        var userId = req.user.sub;
        User.findOne({ _id: userId }, (err, user) => {
          // Check if error was found
          if (err) {
            res.json({ success: false, message: "Something went wrong." }); // Return error message
          }
          // Check if id of user in session was found in the database
          else {
            // Check if the user who liked the post has already liked the service post before
            if (service.accept == "rechazado") {
              service.accept = "aceptado";
              service.backgroundColor = "#00a65a"; // Reduce the total number of likes
              // Save service post data
              service.save((err) => {
                // Check if error was found
                if (err) {
                  res.json({ success: false, message: "Algo malo ocurrio" }); // Return error message
                } else {
                  res.json({
                    success: true,
                    message: "service aceptado!",
                    service,
                  }); // Return success message
                }
              });

              if (service.accept == "espera") {
                service.accept = "aceptado";
                service.backgroundColor = "#00a65a"; // Reduce the total number of likes
                // Save service post data
                service.save((err) => {
                  // Check if error was found
                  if (err) {
                    res.json({ success: false, message: "Algo malo ocurrio" }); // Return error message
                  } else {
                    res.json({
                      success: true,
                      message: "service aceptado!",
                      service,
                    }); // Return success message
                  }
                });
              }
            }
          }
        });
      }
    }
  });
}

function recjetService(req, res) {
  // Search the database with id
  Service.findOne({ _id: req.params.id }, (err, service) => {
    // Check if error was encountered
    if (err) {
      res.json({ success: false, message: "Invalid service id" }); // Return error message
    } else {
      // Check if id matched the id of a service post in the database
      if (!service) {
        res.json({ success: false, message: "That service was not found." }); // Return error message
      } else {
        // Get data from user that is signed
        var userId = req.user.sub;
        User.findOne({ _id: userId }, (err, user) => {
          // Check if error was found
          if (err) {
            res.json({ success: false, message: "Something went wrong." }); // Return error message
          }
          // Check if id of user in session was found in the database
          else {
            // Check if the user who liked the post has already liked the service post before
            if (service.accept == "espera") {
                service.accept = "rechazado";
                service.backgroundColor = "#dd4b39"; // Reduce the total number of likes
                // Save service post data
                service.save((err) => {
                  // Check if error was found
                  if (err) {
                    res.json({ success: false, message: "Algo malo ocurrio" }); // Return error message
                  } else {
                    res.json({
                      success: true,
                      message: "service rechazado!",
                      service,
                    }); // Return success message
                  }
                });
              }

            if (service.accept == "aceptado") {
              service.accept = "rechazado";
              service.backgroundColor = "#dd4b39"; // Reduce the total number of likes
              // Save service post data
              service.save((err) => {
                // Check if error was found
                if (err) {
                  res.json({ success: false, message: "Algo malo ocurrio" }); // Return error message
                } else {
                  res.json({
                    success: true,
                    message: "service rechazado!",
                    service,
                  }); // Return success message
                }
              });
            }
          }
        });
      }
    }
  });
}

module.exports = {
  probando,
  saveService,
  getServicesContractor,
  getServicesProvider,
  getServicesProviderWithId,
  acceptService,
  recjetService,
};
