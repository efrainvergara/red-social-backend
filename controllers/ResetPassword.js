/**
  * Propiedad BOSOCIAL 2020
*/

'use strict'

var User = require('../models/user');
var passwordResetToken = require('../models/passwordResetToken');

var crypto = require('crypto');
var nodemailer = require('nodemailer');
var bcrypt = require('bcrypt-nodejs');

async function ResetPassword(req, res) {
  if (!req.body.email) {
    return res.status(500).json({ message: "correo electronico es requerido" });
  }
  const user = await User.findOne({
    email: req.body.email,
  });
  if (!user) {
    return res.status(409).json({ message: "El correo electrónico no existe" });
  }
  var resettoken = new passwordResetToken({
    _userId: user._id,
    resettoken: crypto.randomBytes(16).toString("hex"),
  });
  resettoken.save(function (err) {
    if (err) {
      return res.status(500).send({ msg: err.message });
    }
    passwordResetToken
      .find({ _userId: user._id, resettoken: { $ne: resettoken.resettoken } })
      .remove()
      .exec();
    res.status(200).json({ message: "Restablecer la contraseña correctamente." });
    var transporter = nodemailer.createTransport({
      service: "Gmail",
      port: 465,
      auth: {
        user: "efrainvergara.udec@gmail.com",
        pass: "ESfa_9669@",
      },
    });
    var mailOptions = {
      to: user.email,
      from: "efrainvergara.udec@gmail.com",
      subject: "Restablecimiento de contraseña BO-Social",
      text:
        "Está recibiendo esto porque usted (u otra persona) ha solicitado el restablecimiento de la contraseña de su cuenta.\n\n" +
        "Haga clic en el siguiente enlace o péguelo en su navegador para completar el proceso:\n\n" +
        "http://localhost:4200/response-reset-password/" +
        resettoken.resettoken +
        "\n\n" +
        "Si no lo solicitó, ignore este correo electrónico y su contraseña permanecerá sin cambios.\n",
    };
    transporter.sendMail(mailOptions, (err, info) => {});
  });
}

async function ValidPasswordToken(req, res) {
  if (!req.body.resettoken) {
    return res.status(500).json({ message: "Se requiere token" });
  }
  const user = await passwordResetToken.findOne({
    resettoken: req.body.resettoken,
  });
  if (!user) {
    return res.status(409).json({ message: "URL invalida" });
  }
  User.findOneAndUpdate({ id: user._userId })
    .then(() => {
      res.status(200).json({ message: "Token verificado correctamente." });
    })
    .catch((err) => {
      return res.status(500).send({ msg: err.message });
    });
}


async function NewPassword(req, res) {
  passwordResetToken.findOne(
    { resettoken: req.body.resettoken },
    function (err, userToken, next) {
      if (!userToken) {
        return res.status(409).json({ message: "El token ha caducado" });
      }

      User.findOne(
        {
          _id: userToken._userId,
        },
        function (err, userEmail, next) {
          if (!userEmail) {
            return res.status(409).json({ message: "El usuario no existe" });
          }
          return bcrypt.hash(req.body.newPassword, null, null,  (err, hash) => {
            if (err) {
              return res
                .status(400)
                .json({ message: "Error al escribir la contraseña" });
            }
            userEmail.password = hash;
            userEmail.save(function (err) {
              if (err) {
                return res
                  .status(400)
                  .json({ message: "No se puede restablecer la contraseña." });
              } else {
                userToken.remove();
                return res
                  .status(201)
                  .json({ message: "Contraseña restablecida con éxito" });
              }
            });
          });
        }
      );
    }
  );
}

module.exports = {
    ResetPassword,
    NewPassword,
    ValidPasswordToken
}
